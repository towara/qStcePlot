#include "mainwindow.h"
#include "qstceplot.h"

class QStcePlot;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    plotWidget = new QStcePlot(parent,true);
    plotWidget->setMinimumSize(640,480);
    lineColor = Qt::red;        

    //verticalLayout->addItem(plotWidget);
    gridLayout = new QGridLayout();
    buttonRed = new QPushButton("Red");
    buttonGreen = new QPushButton("Green");
    buttonBlue = new QPushButton("Blue");

    // checkable Buttons
    buttonRed->setCheckable(true);
    buttonGreen->setCheckable(true);
    buttonBlue->setCheckable(true);

    // only one checkable at a time
    buttonRed->setAutoExclusive(true);
    buttonGreen->setAutoExclusive(true);
    buttonBlue->setAutoExclusive(true);

    // red checked by default
    buttonRed->setChecked(true);

    // Grid Layout 3 Buttons in first row
    gridLayout->addWidget(buttonRed,0,0);
    gridLayout->addWidget(buttonGreen,0,1);
    gridLayout->addWidget(buttonBlue,0,2);
    // one plot widget spanning 3 columns in 2nd row
    gridLayout->addWidget(plotWidget,1,0,1,3);

    // bind gridLayout to MainWindow
    centralWidget = new QWidget();
    centralWidget->setLayout(gridLayout);
    setCentralWidget(centralWidget);

    this->statusBar()->showMessage("");


    QObject::connect(plotWidget,SIGNAL(plotOnClickEvent(double,double,Qt::MouseButton)),this,SLOT(mouseSlot(double,double,Qt::MouseButton)));

    QObject::connect(buttonRed,   SIGNAL(released()),this,SLOT(changeColorSlot()));
    QObject::connect(buttonGreen, SIGNAL(released()),this,SLOT(changeColorSlot()));
    QObject::connect(buttonBlue,  SIGNAL(released()),this,SLOT(changeColorSlot()));
}

void MainWindow::mouseSlot(double x, double y, Qt::MouseButton btn){
    //std::cout << "click at " << x << " | " << y << " btn: " << btn <<std::endl;
    QString str = "Click at (" + QString().setNum(x,'f',2) + " | " + QString().setNum(y,'f',2) + ") with Button " + QString().setNum(btn) +".";
    this->statusBar()->showMessage(str,1000);
    if(btn == 1){
        xPos.append(x);
        yPos.append(y);
        plotWidget->setKeyPoints(xPos,yPos);
        plotWidget->setPoints(xPos,yPos,1,lineColor);
    }
}

void MainWindow::changeColorSlot()
{
    if(buttonRed->isChecked())
        lineColor = Qt::red;
    else if(buttonGreen->isChecked())
        lineColor = Qt::green;
    else if(buttonBlue->isChecked())
        lineColor = Qt::blue;

    plotWidget->setPoints(xPos,yPos,1,lineColor);
}





MainWindow::~MainWindow()
{
  delete plotWidget;
  delete buttonRed;
  delete buttonGreen;
  delete buttonBlue;
  delete gridLayout;
  delete centralWidget;
}
