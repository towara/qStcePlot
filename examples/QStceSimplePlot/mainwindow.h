#ifndef MAINWINDOW_H
#define MAINWINDOW_H

//#include <QtGlobal>
//#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
//# include <QtGui/QApplication>
//# include <QtGui>
//#else
//# include <QtWidgets>
#include <QApplication>
//#endif

#include <QMainWindow>
#include <iostream>
#include <QGridLayout>
#include <QPushButton>
#include <QStatusBar>

class QStcePlot;


/*!
\brief  The MainWindow of the Application

Contains a 2x3 Grid Layout and a Statusbar at the bottom of the Window.
  <table>
  <tr>
    <td align="center">QPushButton</th>
    <td align="center">QPushButton</th>
    <td align="center">QPushButton</th>
  </tr>
  <tr>
    <td align="center" colspan="3">QStcePlot</td>
  </tr>
  </table>
*/
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /*!
     * \brief Constructs the MainWindow.
     * \param parent
     */
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    //Ui::MainWindow *ui;
    QVector<double> xPos,yPos;

    // UI Elements
    QStcePlot* plotWidget;

    QGridLayout* gridLayout;
    QWidget*     centralWidget;

    QPushButton* buttonRed;
    QPushButton* buttonGreen;
    QPushButton* buttonBlue;

    QColor lineColor;

public slots:
    /*!
     * \brief public slot mouseSlot gets called when plotWidget is clicked.
     * \param x x-Coordinate in Plot-Coordinates.
     * \param y y-Coordinate in Plot-Coordinates.
     * \param btn Pressed Mousebutton.
     */
    void mouseSlot(double x, double y, Qt::MouseButton btn);
    /*!
     * \brief Handles the color change when one of the PushButtons is pressed.
     * This slot should be connected with the release() signal of QPushButton
     */
    void changeColorSlot();
};

#endif // MAINWINDOW_H
