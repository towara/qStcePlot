#-------------------------------------------------
#
# Project created by QtCreator 2013-04-05T10:11:32
#
#-------------------------------------------------

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4) {
    QT += widgets printsupport
}

TARGET =           qtWorker
TEMPLATE =         app

CONFIG +=          release

INCLUDEPATH +=     ../../inc

SOURCES +=         main.cpp\
                   mainwindow.cpp \
                   ../../src/qcustomplot.cpp \
                   ../../src/qstceplot.cpp \
                   worker.cpp

HEADERS +=         mainwindow.h \
                   ../../inc/qcustomplot.h \
                   ../../inc/qstceplot.h \
                   worker.h

QMAKE_CXXFLAGS +=
