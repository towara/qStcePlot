#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGlobal>
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
# include <QtGui>
#else
# include <QtWidgets>
#endif

//#include <QMainWindow>
#include <iostream>
#include <QVector>
#include <QList>

class Worker;
class QStcePlot;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent *event);

private:
    Worker* _worker;

    QStcePlot* plotWidget;

    bool _stopFlag;
    // store values here
    QVector<double> _x;
    QVector<double> _y;
    double _angle;
    void spawnNewWorker();

public slots:
    void buttonSlot(double x, double y, Qt::MouseButton btn);
    void updateResultSlot();
    //void workerFinishedSlot();
};

#endif // MAINWINDOW_H
