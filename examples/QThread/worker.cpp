#include "worker.h"

#define PI 3.141592654

inline void getArcCoords(double angle, double diam, double& x, double& y){
    x = sin(angle)*diam;
    y = cos(angle)*diam;
}

// --- CONSTRUCTOR ---
Worker::Worker(QVector<double>& x, QVector<double>& y, double& angle, bool& stopFlag) :
    _x(x),
    _y(y),
    _angle(angle),
    _stopFlag(stopFlag)
{
    _diam = 20;

    // Point markers
    _x.clear();_y.clear();
    _x.append(0);_x.append(0);_x.append(0);_x.append(0);
    _y.append(0);_y.append(0);_y.append(0);_y.append(0);
}


// --- DECONSTRUCTOR ---
Worker::~Worker() {
    // free resources
}

// --- PROCESS ---
// Start processing data.
void Worker::run() {
    // allocate resources using new here
    while(true){
        if(_stopFlag)
            break;

        QThread::msleep(25);
        _angle += 0.01;

        getArcCoords(_angle               ,_diam,_x[0],_y[0]);
        getArcCoords(_angle + 2*PI*1.0/3.0,_diam,_x[1],_y[1]);
        getArcCoords(_angle + 2*PI*2.0/3.0,_diam,_x[2],_y[2]);
        getArcCoords(_angle               ,_diam,_x[3],_y[3]);

        // send signal to main thread
        emit updateResult();        
    }
    emit finished();
}
