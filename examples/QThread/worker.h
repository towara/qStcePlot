#ifndef WORKER_H
#define WORKER_H
#include <QObject>
#include <QVector>
#include <QThread>
#include "math.h"

using namespace std;
class Worker : public QThread {
    Q_OBJECT

public:
    Worker(QVector<double>& xList, QVector<double>& yList, double& angle, bool& stopFlag);
    ~Worker();

public slots:
    void run();

signals:
    void finished();
    void updateResult();
    void error(QString err);

private:
    bool& _stopFlag;
    double _diam;
    double& _angle;
    // references to the list of the main thread
    // so we don't have to copy data around
    QVector<double>& _x;
    QVector<double>& _y;
};

#endif // WORKER_H
