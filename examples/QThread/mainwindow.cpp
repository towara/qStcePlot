#include "mainwindow.h"
#include "worker.h"
#include "qstceplot.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    plotWidget = new QStcePlot(parent,false);
    plotWidget->setMinimumSize(640,480);
    plotWidget->setRange(-40,40,-30,30);
    this->setCentralWidget(plotWidget);

    QObject::connect(plotWidget,SIGNAL(plotOnClickEvent(double,double,Qt::MouseButton)),this,SLOT(buttonSlot(double,double,Qt::MouseButton)));

    // Setup Worker Thread
    _angle = 0;
    spawnNewWorker();
}

void MainWindow::updateResultSlot(){
    plotWidget->setKeyPoints(_x,_y);
    plotWidget->setPoints(_x,_y,1,Qt::red);
    plotWidget->replot();
}

void MainWindow::buttonSlot(double x, double y, Qt::MouseButton btn){
    if(_stopFlag){
        spawnNewWorker();
    }else{
        _stopFlag=true;
    }
}

void MainWindow::closeEvent(QCloseEvent *event){
    // set stopFlag so worker thread knows to stop when window is closed
    _stopFlag = true;
    // wait for thread to be finished
    _worker->wait();
}

void MainWindow::spawnNewWorker()
{
    _stopFlag=false;
    // New worker Thread
    _worker = new Worker(_x,_y,_angle,_stopFlag);
    //_thread = new std::thread(&Worker::run,_worker,std::ref(_stopFlag));
    // connect worker and main thread
    connect(_worker,SIGNAL(updateResult()),this,SLOT(updateResultSlot()));
    connect(_worker,SIGNAL(finished()),_worker, SLOT(deleteLater()));
    _worker->start();
}

MainWindow::~MainWindow()
{

}
