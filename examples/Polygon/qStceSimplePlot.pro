#-------------------------------------------------
#
# Project created by QtCreator 2013-04-05T10:11:32
#
#-------------------------------------------------

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4) {
    QT += widgets printsupport
}

TARGET =   qStceSimplePlot
TEMPLATE = app

CONFIG += debug

INCLUDEPATH += ../../inc

SOURCES += main.cpp\
           mainwindow.cpp \
           ../../src/qcustomplot.cpp \
           ../../src/qstceplot.cpp

HEADERS += mainwindow.h \
           ../../inc/qcustomplot.h \
           ../../inc/qstceplot.h
