#include <QtGlobal>
#include <QApplication>

#include "mainwindow.h"

/*! \mainpage %QStceSimplePlot Documentation

  This is the Documentation for QStceSimplePlot.
  \see MainWindow
  \see QStcePlot
*/

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
