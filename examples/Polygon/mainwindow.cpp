#include "mainwindow.h"
#include "qstceplot.h"

class QStcePlot;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    plotWidget_ = new QStcePlot(parent,true);
    plotWidget_->setMinimumSize(640,480);
    plotWidget_->setRange(0,7,0,7);
    QVector<double> x,y;
    this->setCentralWidget(plotWidget_);

    x.push_back(1); y.push_back(1); // P1 (1,1)
    x.push_back(2); y.push_back(2); // P2 (2,2)
    x.push_back(3); y.push_back(1); // P3 (3,1)
    x.push_back(1); y.push_back(1); // P4 (1,1) close triangle

    x.push_back(qQNaN()); y.push_back(1); // invalid coordinate, tells plot engine to start new polygon

    x.push_back(4); y.push_back(1); // P1 (4,1)
    x.push_back(4); y.push_back(3); // P2 (4,3)
    x.push_back(6); y.push_back(3); // P3 (6,3)
    x.push_back(6); y.push_back(1); // P4 (6,1)
    x.push_back(4); y.push_back(1); // P5 (4,1) close quad

    plotWidget_->plotPolygon(x,y,Qt::black,QColor(255,0,0,50)); // set from rgba color value
}

MainWindow::~MainWindow()
{
  delete plotWidget_;
}
