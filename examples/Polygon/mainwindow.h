#ifndef MAINWINDOW_H
#define MAINWINDOW_H

//#include <QtGlobal>
//#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
//# include <QtGui/QApplication>
//# include <QtGui>
//#else
//# include <QtWidgets>
#include <QApplication>
//#endif

#include <QMainWindow>
#include <iostream>
#include <QHBoxLayout>


class QStcePlot;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /*!
     * \brief Constructs the MainWindow.
     * \param parent
     */
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    // UI Elements
    QStcePlot* plotWidget_;
};

#endif // MAINWINDOW_H
